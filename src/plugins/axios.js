import axios from 'axios'
import Vue from 'vue'

const instance = axios.create({ baseURL: 'https://api.themoviedb.org/3/' })

Vue.prototype.$axios = instance
