import { API_KEY } from '../resources/data'

export default {
  methods: {
    getMoviePoster: function (path) {
      return `https://image.tmdb.org/t/p/w500${path}`
    },
    getTrendingMovies: function (onSuccess = () => null, onError = () => null) {
      return this.$axios.get('trending/movie/week', { params: { api_key: API_KEY } }).then(({ data }) => {
        onSuccess(data)
      }).catch((err) => {
        onError(err)
      })
    },
    getMovieDetails: function (movieId, onSuccess = () => null, onError = () => null) {
      return this.$axios.get(`movie/${movieId}`, { params: { api_key: API_KEY } }).then(({ data }) => {
        onSuccess(data)
      }).catch((err) => {
        onError(err)
      })
    },
    getMovieTrailer: function (movieId, onSuccess = () => null, onError = () => null) {
      return this.$axios.get(`movie/${movieId}/videos`, { params: { api_key: API_KEY } }).then(({ data }) => {
        onSuccess(data)
      }).catch((err) => {
        onError(err)
      })
    }
  }
}
