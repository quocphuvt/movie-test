import Vue from 'vue'
import Vuex from 'vuex'
import * as types from './types'

import layout from './layout/index'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    favoriteMovies: []
  },
  actions: {
    addFavoriteMovies ({ state, commit, dispatch }, movie) {
      if (state.favoriteMovies.some(item => item.id === movie.id)) {
        return
      }

      commit(types.ADD_FAVORITE_MOVIE, movie)
      dispatch('layout/openSidebar')
    },
    removeFavoriteMovies ({ state, commit }, movieId) {
      if (state.favoriteMovies.some(item => item.id === movieId)) {
        commit(types.REMOVE_FAVORITE_MOVIE, movieId)
      }
    }
  },
  mutations: {
    [types.ADD_FAVORITE_MOVIE] (state, movie) {
      state.favoriteMovies.unshift(movie)
    },
    [types.REMOVE_FAVORITE_MOVIE] (state, movieId) {
      const index = state.favoriteMovies.findIndex(movie => movie.id === movieId)
      state.favoriteMovies.splice(index, 1)
    }
  },
  getters: {
    totalFavoriteMovies: (state) => state.favoriteMovies.length,
    isFavorite: (state) => movieId => {
      return state.favoriteMovies.some(movie => movie.id === movieId)
    }
  },
  modules: {
    layout
  }
})

export default store
