import { CLOSE_MODAL, CLOSE_SIDEBAR, OPEN_MODAL, OPEN_SIDEBAR } from './types'

export default {
  openModal ({ commit }, modalProps) {
    commit(OPEN_MODAL, modalProps)
  },
  closeModal ({ commit }) {
    commit(CLOSE_MODAL)
  },
  openSidebar ({ commit }) {
    commit(OPEN_SIDEBAR)
  },
  closeSidebar ({ commit }) {
    commit(CLOSE_SIDEBAR)
  }
}
