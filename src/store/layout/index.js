import state from './state'
import actions from './action'
import mutations from './mutation'

export default {
  state,
  actions,
  mutations,
  namespaced: true
}
