export const defaultModalStates = {
  isOpen: false,
  component: null,
  props: null,
  styles: {
    size: 'md'
  }
}

export const defaultSidebarStates = {
  isOpen: false
}

export default {
  modal: { ...defaultModalStates },
  sidebar: { ...defaultSidebarStates }
}
