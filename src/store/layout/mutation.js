import { defaultModalStates } from './state'
import { CLOSE_MODAL, OPEN_MODAL, OPEN_SIDEBAR, CLOSE_SIDEBAR } from './types'

export default {
  [OPEN_MODAL] (state, modalProps) {
    state.modal.isOpen = true
    state.modal.component = modalProps.component
    state.modal.props = modalProps.props
    if (modalProps.styles) {
      state.modal.styles = modalProps.styles
    }
  },
  [CLOSE_MODAL] (state) {
    state.modal = { ...defaultModalStates }
  },
  [OPEN_SIDEBAR] (state) {
    state.sidebar.isOpen = true
  },
  [CLOSE_SIDEBAR] (state) {
    state.sidebar.isOpen = false
  }
}
