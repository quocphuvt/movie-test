import Vue from 'vue'
import store from './store/index'

import App from './App.vue'
import './plugins/axios'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import './assets/scss/_vars.scss'
import './assets/scss/index.scss'

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  store: store
}).$mount('#app')
